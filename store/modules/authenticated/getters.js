import Constants from './constants'
import state from './state'

export default {
    [Constants.GET_NAME]: (state) => {
        return state.name
    }
}
